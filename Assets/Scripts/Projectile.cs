using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    private Monster _target;
    private Tower _tower;
    private float _speed;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    private void Update()
    {
        MoveToTarget();
        CheckToGoal();
    }

    public void Initialize(Monster target, Tower source)
    {
        _target = target;
        _tower = source;
        _speed = source.ProjectSpeed;
        
        transform.position = source.transform.position;
    }

    private void MoveToTarget()
    {
        if (_target == null) return;
        
        var sourcePos = transform.position;

        var targetPos = _target.transform.position;
        
        sourcePos = Vector3.MoveTowards(sourcePos, targetPos, _speed * Time.deltaTime);
        transform.position = sourcePos;

        Vector2 dir = targetPos - sourcePos;
        var angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (!other.TryGetComponent(out Monster monster)) return;
        if (_target != monster) return;
        _target.TakeDamage(_tower.Damage);
        ApplyDebuff();
        GameManager.Instance.Pool.ReturnObject(gameObject);
    }

    private void CheckToGoal()
    {
        var distance = Vector3.Distance(transform.position, _target.transform.position);
        if(distance <= 0) GameManager.Instance.Pool.ReturnObject(gameObject);
    }

    private void ApplyDebuff()
    {
        if (_tower.GetDebuff() == null) return;
        _target.AddDebuff(_tower.GetDebuff());
    }
}
