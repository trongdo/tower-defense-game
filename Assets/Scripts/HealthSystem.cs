using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthSystem : MonoBehaviour
{
    [SerializeField] private float maxValue;
    [SerializeField] private float currentValue;

    public int MaxValue
    {
        set
        {
            maxValue = value;
            currentValue = value;
            healthBar.SetMaxHealth(maxValue);
        }
    }

    public float CurrentValue => currentValue;

    [SerializeField] private HealthBar healthBar;
    

    public void DecreaseHealth(float damage)
    {
        currentValue -= damage;
        healthBar.SetHealth(currentValue);
        if (currentValue < 0) currentValue = 0;
    }
}
