﻿using System;
using UnityEngine;


public class Node
{
    public Point GridPosition { get; private set; }

    public TileScript TileRef { get; private set; }
    
    public Vector2 WorldPosition { get; private set; }

    public Node Parent { get; private set; }

    public int G { get; set; }
    public int H { get; set; }
    public int F { get; set; }
    
    public Node(TileScript tileRef)
    {
        TileRef = tileRef;
        GridPosition = tileRef.GridPosition;
        WorldPosition = tileRef.WorldPosition;
    }

    public void CalValue(Node parent, Node goal, int gCost)
    {
        Parent = parent;
        G = parent.G + gCost;
        H = (Math.Abs(goal.GridPosition.X - GridPosition.X) +
             Math.Abs(goal.GridPosition.Y - GridPosition.Y)) * 10;
        F = G + H;
    }
}
