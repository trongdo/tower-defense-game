﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class AStar
{
    private const int GCOST_FOR_DIRECT_MOVING = 10;
    private const int GCOST_FOR_DIAGONALLY_MOVING = 14;
    private static Dictionary<Point, Node> _nodes;

    private static void CreateNodes()
    {
        _nodes = new Dictionary<Point, Node>();

        foreach (var tile in LevelManager.Instance.Tiles.Values)
        {
            _nodes.Add(tile.GridPosition, new Node(tile));
        }
    }

    public static Stack<Node> GetPath(Point start, Point goal)
    {
        CreateNodes();

        var openList = new HashSet<Node>();
        var closedList = new HashSet<Node>();
        var finalPath = new Stack<Node>();

        var currentNode = _nodes[start];
        openList.Add(currentNode);

        while (openList.Count > 0)
        {
            //Run through all the neighbors 
            for (int x = -1; x <= 1; x++)
            {
                for (int y = -1; y <= 1; y++)
                {
                    var neighbourPos = new Point(currentNode.GridPosition.X - x, currentNode.GridPosition.Y - y);
                    
                    //Check if the neighbour node is in bound of the map, if not ignore it
                    if (!_nodes.ContainsKey(neighbourPos)) continue;
                    
                    //Check if the neighbour node is walkable or is the current node, if is not walkable and is the current node ignore it
                    if (neighbourPos == currentNode.GridPosition ||
                        !LevelManager.Instance.Tiles[neighbourPos].Walkable) continue;
                    
                    //Initialize the G cost 
                    var gCost = 0;
                    if (Math.Abs(x - y) == 1)
                    {
                        gCost = GCOST_FOR_DIRECT_MOVING;
                    }
                    else
                    {
                        if (!CheckCanPassDiagonally(currentNode, _nodes[neighbourPos])) continue;
                        gCost = GCOST_FOR_DIAGONALLY_MOVING;
                    }

                    var neighbour = _nodes[neighbourPos];

                    if (openList.Contains(neighbour))
                    {
                        if (currentNode.G + gCost < neighbour.G)
                        {
                            neighbour.CalValue(currentNode, _nodes[goal], gCost);
                        }
                    }
                    else if (!closedList.Contains(neighbour))
                    {
                        openList.Add(neighbour);
                        neighbour.CalValue(currentNode, _nodes[goal], gCost);
                    }
                }
            }

            openList.Remove(currentNode);
            closedList.Add(currentNode);

            if (openList.Count > 0)
            {
                currentNode = openList.OrderBy(n => n.F).First();
            }

            if (currentNode != _nodes[goal]) continue;
            
            while (currentNode != _nodes[start])
            {
                finalPath.Push(currentNode); 
                currentNode = currentNode.Parent;
            }

            return finalPath;
        }

        // DEBUG SECTION, DELETE LATER
        //GameObject.Find("AStarDebugger").GetComponent<AStarDebugger>().DebugPath(openList, closedList, finalPath);
        
        return null;
    }

    private static bool CheckCanPassDiagonally(Node currentNode, Node neighbourNode)
    {
        var fist = new Point(currentNode.GridPosition.X, neighbourNode.GridPosition.Y);
        var second = new Point(neighbourNode.GridPosition.X, currentNode.GridPosition.Y);

        return _nodes[fist].TileRef.Walkable && _nodes[second].TileRef.Walkable;
    }
}