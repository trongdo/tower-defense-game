﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.Mathematics;
using UnityEngine;

public class AStarDebugger : MonoBehaviour
{
    [SerializeField] private TileScript _start;
    [SerializeField] private TileScript _goal;
    [SerializeField] private GameObject _arrowPrefab;
    [SerializeField] private GameObject _debugTilePrefab;
    [SerializeField] private GameObject _debugLayer;
    
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        ClickTile();

        if (Input.GetKeyDown(KeyCode.Space))
        {
            AStar.GetPath(_start.GridPosition, _goal.GridPosition);
        }
    }

    private void ClickTile()
    {
        if (!Input.GetMouseButtonDown(1)) return;
        
        var hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
        var temp = hit.collider.GetComponent<TileScript>();

        if (!temp) return;
        
        if (_start == null)
        {
            _start = temp;
            PlaceDebugTile(_start.WorldPosition, Color.green);
        }
        else if (_goal == null)
        {
            _goal = temp;
            PlaceDebugTile(_goal.WorldPosition, Color.red);
        }
    }

    public void DebugPath(HashSet<Node> openList, HashSet<Node> closedList, Stack<Node> finalPath)
    {
        foreach (var node in openList.Where(node => node.TileRef != _start && node.TileRef != _goal))
        {
            PlaceDebugTile(node.TileRef.WorldPosition, Color.gray, node);
            
            PointToParent(node, node.TileRef.WorldPosition);
        }

        foreach (var node in closedList.Where(node => node.TileRef != _goal && node.TileRef != _start))
        {
            PlaceDebugTile(node.TileRef.WorldPosition, Color.yellow, node);
        }

        foreach (var node in finalPath.Where(node => node.TileRef != _goal))
        {
            PlaceDebugTile(node.TileRef.WorldPosition, Color.blue);
        }
    }

    private void PointToParent(Node node, Vector2 position)
    {
        if (node.Parent == null) return;
        
        var arrow = Instantiate(_arrowPrefab, position, Quaternion.identity);
        arrow.transform.parent = _debugLayer.transform;
        arrow.GetComponent<SpriteRenderer>().sortingOrder = 3;
        
        if (node.GridPosition.X < node.Parent.GridPosition.X && node.GridPosition.Y == node.Parent.GridPosition.Y)
        {
            arrow.transform.eulerAngles = new Vector3(0, 0, 0);
        }
        else if (node.GridPosition.X < node.Parent.GridPosition.X && node.GridPosition.Y > node.Parent.GridPosition.Y)
        {
            arrow.transform.eulerAngles = new Vector3(0, 0, 45);
        }
        else if (node.GridPosition.X == node.Parent.GridPosition.X && node.GridPosition.Y > node.Parent.GridPosition.Y)
        {
            arrow.transform.eulerAngles = new Vector3(0, 0, 90);
        }
        else if (node.GridPosition.X > node.Parent.GridPosition.X && node.GridPosition.Y > node.Parent.GridPosition.Y)
        {
            arrow.transform.eulerAngles = new Vector3(0, 0, 135);
        }
        else if (node.GridPosition.X > node.Parent.GridPosition.X && node.GridPosition.Y == node.Parent.GridPosition.Y)
        {
            arrow.transform.eulerAngles = new Vector3(0, 0, 180);
        }
        else if (node.GridPosition.X > node.Parent.GridPosition.X && node.GridPosition.Y < node.Parent.GridPosition.Y)
        {
            arrow.transform.eulerAngles = new Vector3(0, 0, 225);
        }
        else if (node.GridPosition.X == node.Parent.GridPosition.X && node.GridPosition.Y < node.Parent.GridPosition.Y)
        {
            arrow.transform.eulerAngles = new Vector3(0, 0, 270);
        }
        else if (node.GridPosition.X < node.Parent.GridPosition.X && node.GridPosition.Y < node.Parent.GridPosition.Y)
        {
            arrow.transform.eulerAngles = new Vector3(0, 0, 315);
        }
    }

    private void PlaceDebugTile(Vector2 position, Color color, Node node = null)
    {
        var debugTile = Instantiate(_debugTilePrefab, position, Quaternion.identity);
        debugTile.transform.SetParent(_debugLayer.transform);
        debugTile.GetComponent<SpriteRenderer>().color = color;

        if (node == null) return;

        debugTile.GetComponent<DebugTile>().G.text += node.G;
        debugTile.GetComponent<DebugTile>().H.text += node.H;
        debugTile.GetComponent<DebugTile>().F.text += node.F;
    }
}
