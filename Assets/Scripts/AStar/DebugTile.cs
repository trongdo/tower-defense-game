﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DebugTile : MonoBehaviour
{
    [SerializeField] private Text _g, _h, _f;

    public Text G
    {
        get
        {
            _g.gameObject.SetActive(true);
            return _g;
        }
        set => _g = value;
    }

    public Text H
    {
        get
        {
            _h.gameObject.SetActive(true);
            return _h;
        }
        set => _h = value;
    }

    public Text F
    {
        get
        {
            _f.gameObject.SetActive(true);
            return _f;
        }
        set => _f = value;
    }
}
