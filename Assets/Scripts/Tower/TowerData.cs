using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class TowerData
{
    [SerializeField] private Tower tower;
    [SerializeField] private string towerName;
    [SerializeField] private string debuffName;
    public Tower Tower => tower;

    public string TowerName => towerName;

    public string DebuffName => debuffName;
}
