using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowTower : Tower
{
    protected override void Awake()
    {
        TowerType = TowerType.ArrowTower;
        TowerUpgrades = new[]
            {new TowerUpgrade(1, 1f, -0.1f, 0.5f), new TowerUpgrade(2, 1.5f, -0.1f,0.5f), new TowerUpgrade(3, 2f, -0.1f,0.5f)};
        base.Awake();
    }

    public override Debuff GetDebuff()
    {
        return null;
    }
}
