using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WizardTower : Tower
{
    [SerializeField] private float slowingFactor;
    protected override void Awake()
    {
        TowerType = TowerType.WizardTower;
        TowerUpgrades = new[]
            {new TowerUpgrade(2, 0.5f, -0.1f, 1f, 5), new TowerUpgrade(3, 0.5f, -0.1f,1f, 10), new TowerUpgrade(4, 0.5f, -0.1f,1f, 15)};

        base.Awake();
    }

    public override Debuff GetDebuff()
    {
        return new SlowDebuff(slowingFactor, _target, DebuffDuration);
    }

    public override void Upgrade()
    {
        slowingFactor += NextUpgrade.SlowFactor;
        base.Upgrade();
    }
}
