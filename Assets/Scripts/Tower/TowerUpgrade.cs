using System.Collections;
using System.Collections.Generic;


public struct TowerUpgrade
{
    public int Price { get; private set; }
    public float Damage { get; private set; }
    public float AttackRate { get; private set; }
    public float Range { get; private set; }
    public int SlowFactor { get; private set; }

    public TowerUpgrade(int price, float damage, float attackRate,float range)
    {
        Price = price;
        Damage = damage;
        AttackRate = attackRate;
        Range = range;
        SlowFactor = 0;
    }

    public TowerUpgrade(int price, float damage, float attackRate, float range, int slowFactor)
    {
        Price = price;
        Damage = damage;
        AttackRate = attackRate;
        Range = range;
        SlowFactor = slowFactor;
    }
}