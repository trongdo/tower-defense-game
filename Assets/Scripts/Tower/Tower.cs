using System;
using System.Collections.Generic;
using System.Drawing;
using UnityEngine;


    public enum TowerType
    {
        ArrowTower,
        WizardTower
    }
    
    public abstract class  Tower : MonoBehaviour
    {
        private SpriteRenderer _rangeSpriteRenderer;
        protected Monster _target;
        private float _attackTimer = 0;
        private TileScript _tile;
        
        [SerializeField] private int price;
        [SerializeField] private float damage;
        [SerializeField] private float range;
        [SerializeField] private string projectileName;
        [SerializeField] private float projectSpeed;
        [SerializeField] private float attackRate;
        [SerializeField] private float debuffDuration;
        [SerializeField] private string shootSfxName;

        public int Level { get; private set; }
        
        public TowerUpgrade[] TowerUpgrades { get; protected set; }

        public TowerUpgrade NextUpgrade => TowerUpgrades.Length > Level - 1 ? TowerUpgrades[Level - 1] : default;

        public TowerType TowerType { get; protected set; }

        public float DebuffDuration => debuffDuration;

        public TileScript Tile
        {
            set => _tile = value;
        }
        public int Price => price;

        public float Range => range;

        public float AttackRate => attackRate;

        public float ProjectSpeed => projectSpeed;

        public float Damage => damage;
        
        protected virtual void Awake()
        {
            _rangeSpriteRenderer = transform.GetChild(0).GetComponent<SpriteRenderer>();
            _rangeSpriteRenderer.transform.localScale = Vector3.one * Range;
            Level = 1;
        }
        
        private void Update()
        {
            Shoot();
        }

        public abstract Debuff GetDebuff();

        public void Select()
        {
            _rangeSpriteRenderer.enabled = !_rangeSpriteRenderer.enabled;
        }

        private Monster GetClosestMonster()
        {
            return Monster.GetClosestMonster(transform.position, range);
        }

        protected virtual void Shoot()
        {
            if (!(Time.time > _attackTimer)) return;
            _attackTimer = Time.time + attackRate;

            _target = GetClosestMonster();
            if (_target == null) return;
        
            var newProjectile = GameManager.Instance.Pool.GetObject(projectileName).GetComponent<Projectile>();
            newProjectile.Initialize(_target, this);
            //PlaySFX
            if (shootSfxName.Length == 0) return;
            SoundManager.Instance.PlaySfx(shootSfxName);
        }

        public void Sell()
        {
            _tile.Walkable = true;
            _tile.IsEmpty = true;
            Destroy(gameObject);
            GameManager.Instance.Currency += price / 2;
        }

        public virtual void Upgrade()
        {
            GameManager.Instance.Currency -= NextUpgrade.Price;
            price += NextUpgrade.Price;
            damage += NextUpgrade.Damage;
            attackRate += NextUpgrade.AttackRate;
            range += NextUpgrade.Range;
            RefreshRangeIndicator();
            Level++;
        }

        private void RefreshRangeIndicator()
        {
            _rangeSpriteRenderer.transform.localScale = Vector3.one * range;
        }
    }

