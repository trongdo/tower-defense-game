using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatPanel : MonoBehaviour
{
    private Text _titleTxt;
    private Text _statTxt;

    private void Awake()
    {
        _titleTxt = transform.Find("TitleTxt").GetComponent<Text>();
        _statTxt = transform.Find("StatTxt").GetComponent<Text>(); 
        gameObject.SetActive(false);
    }

    public void ToggleShow(TowerData towerData)
    {
        _titleTxt.text = towerData.TowerName;
        _statTxt.text = string.Format("- Range: {0}\n- Damage: {1}\n- Attack Rate: {2}\n- Debuff: {3}", towerData.Tower.Range, towerData.Tower.Damage, towerData.Tower.AttackRate, towerData.DebuffName);
        gameObject.SetActive(!gameObject.activeSelf);
    }
}
