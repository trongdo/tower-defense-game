using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlowDebuff : Debuff
{
    private float _slowingFactor;
    private bool _isApplied;
    public SlowDebuff(float slowingFactor, Monster target, float duration) : base(target, duration)
    {
        _slowingFactor = slowingFactor;
    }

    public override void Update()
    {
        if (Target != null)
        {
            if (!_isApplied)
            {
                _isApplied = true;
                Target.Speed -= (Target.Speed * _slowingFactor) / 100;
            }
        }
        base.Update();
    }

    public override void Remove()
    {
        Target.Speed = Target.MaxSpeed;
        base.Remove();
    }
}
