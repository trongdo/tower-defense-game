using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PlayerLoop;

public abstract class Debuff
{
    protected Monster Target;
    private float _duration;
    private float _elapsedTime = 0f;

    public Debuff(Monster target, float duration)
    {
        Target = target;
        _duration = duration;
    }

    public virtual void Update()
    {
        _elapsedTime += Time.deltaTime;
        if (_elapsedTime >= _duration)
        {
            Remove();
            _elapsedTime = 0f;
        }
    }

    public virtual void Remove()
    {
        if (Target == null) return;
        Target.RemoveDebuff(this);
    }
}
