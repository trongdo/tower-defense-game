using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Testing : MonoBehaviour
{
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            int testCount = 5;
            float startTime = Time.realtimeSinceStartup;
            for (int i = 0; i < testCount; i++)
            {
                AStar.GetPath(new Point(0, 0), new Point(15, 8));
                Debug.Log((Time.realtimeSinceStartup - startTime) * 1000f);
            }
            
        }
    }
}
