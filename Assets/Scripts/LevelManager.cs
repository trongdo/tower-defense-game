﻿using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

public class LevelManager : Singleton<LevelManager>
{
    [SerializeField] private GameObject _tile = null;
    [SerializeField] private int _mapWidth = 0;
    [SerializeField] private int _mapHeight = 0;
    [SerializeField] private CameraMovement _cameraMovement = null;
    [SerializeField] private GameObject _redPortalPrefab = null;
    [SerializeField] private GameObject bluePortalPrefab = null;
    [SerializeField] private Transform _tileMap = null;
    [SerializeField] private Point redPortalPos;
    [SerializeField] private Point bluePortalPos;
    private Stack<Node> path;

    public Stack<Node> Path => new Stack<Node>(new Stack<Node>(path));

    public Portal RedPortal { get; private set; }
    public Portal BluePortal { get; private set; }

    public Point RedPortalPos => redPortalPos;

    public Point BluePortalPos => bluePortalPos;

    public Dictionary<Point, TileScript> Tiles { get; private set; }
    

    private float _tileSize;
    
    private void Start()
    {
        _tileSize = _tile.GetComponent<SpriteRenderer>().sprite.bounds.size.x;
        CreateLevel();
    }

    private void CreateLevel()
    {
        Tiles = new Dictionary<Point, TileScript>();
        var worldStartPos = Camera.main.ScreenToWorldPoint(new Vector3(0, Screen.height, 0));

        for (var y = 0; y < _mapHeight; y++)
        {
            for (var x = 0; x < _mapWidth; x++)
            {
                PlaceTile(x, y, worldStartPos);
            }
        }
        
        var maxTile = Tiles[new Point(_mapWidth - 1, _mapHeight - 1)].transform.position;
        _cameraMovement.SetCameraLimit(new Vector3(maxTile.x + _tileSize, maxTile.y - _tileSize));
        
        PlacePortal();
    }

    private void PlaceTile(int x, int y, Vector3 worldStartPos )
    {
        var newTile = Instantiate(_tile).GetComponent<TileScript>();
        newTile.Setup(new Point(x, y), new Vector3(worldStartPos.x + (x * _tileSize), worldStartPos.y - (y * _tileSize), 0), _tileMap);
    }

    private void PlacePortal()
    {
        RedPortal = Instantiate(_redPortalPrefab, Tiles[redPortalPos].WorldPosition, quaternion.identity).GetComponent<Portal>();
        BluePortal = Instantiate(bluePortalPrefab, Tiles[bluePortalPos].WorldPosition, quaternion.identity)
            .GetComponent<Portal>();
    }

    public void GeneratePath()
    {
        path = AStar.GetPath(redPortalPos, bluePortalPos);
    }
}
