using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundManager : Singleton<SoundManager>
{
    private const string MUSIC_VOLUME = "Music";
    private const string SFX_VOLUME = "Sfx";
    [SerializeField] private AudioSource musicSource;
    [SerializeField] private AudioSource sfxSource;
    [SerializeField] private Slider musicSlider;
    [SerializeField] private Slider sfxSlider;
    
    private Dictionary<string, AudioClip> _audioClips = new Dictionary<string, AudioClip>();

    private void Awake()
    {
        var audioClips = Resources.LoadAll<AudioClip>("Audio");
        foreach (var audioClip in audioClips)
        {
            _audioClips.Add(audioClip.name, audioClip);
        }
        
        LoadVolume();
    }

    private void Start()
    {
        musicSlider.onValueChanged.AddListener(delegate { UpdateVolume(); });
        sfxSlider.onValueChanged.AddListener(delegate { UpdateVolume(); });
    }

    public void PlaySfx(string sound)
    {
        sfxSource.PlayOneShot(_audioClips[sound]);
    }

    public void ChangeMusic(string sound)
    {
        musicSource.clip = _audioClips[sound];
        musicSource.Play();
    }

    public void UpdateVolume()
    {
        musicSource.volume = musicSlider.value;
        sfxSource.volume = sfxSlider.value;
        
        PlayerPrefs.SetFloat(MUSIC_VOLUME, musicSlider.value);
        PlayerPrefs.SetFloat(SFX_VOLUME, sfxSlider.value);
    }

    private void LoadVolume()
    {
        musicSource.volume = PlayerPrefs.GetFloat(MUSIC_VOLUME, 1f);
        sfxSource.volume = PlayerPrefs.GetFloat(SFX_VOLUME, 1f);

        musicSlider.value = musicSource.volume;
        sfxSlider.value = sfxSource.volume;
    }
}
