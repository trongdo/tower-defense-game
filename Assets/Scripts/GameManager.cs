﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public delegate void CurrencyChangedDelegate();

public class GameManager : Singleton<GameManager>
{
    public event CurrencyChangedDelegate OnCurrencyChanged;
    
    public ObjectPool Pool { get; set; }
    private int _currency;
    private int _waveNumber;
    private Tower  _selectedTower;
    
    [Header("UI Reference")]
    [SerializeField] private GameObject gameOverUI;
    [SerializeField] private GameObject pauseMenu;
    [SerializeField] private GameObject optionsMenu;
    [SerializeField] private GameObject upgradePanel;
    [SerializeField] private GameObject towerPanel;
    [SerializeField] private StatPanel statPanel;
    [SerializeField] private Text currencyTxt;
    [SerializeField] private Text waveTxt;
    [SerializeField] private GameObject startWaveBtn;
    [SerializeField] private Text sellTxt;
    [SerializeField] private Text upgradePriceTxt;

    [Header("Game config variable")]
    [Range(1, 5)][SerializeField] private float spawnRate;
    [SerializeField] private int initialCurrency;
    [SerializeField] private int monsterHealth;

    public int SpawnMonster { get; set; } = 0;

    public int Currency
    {
        get => _currency;

        set
        {
            _currency = value;
            if (_currency <= 0)
            {
                _currency = 0;
                ShowGameOver();
            }
            currencyTxt.text = _currency + "<color=lime>$</color>";
            OnCurrencyChanged?.Invoke();
        }
    }

    public TowerButton ClickedButton { get; set; }

    private void Awake()
    {
        Time.timeScale = 1;
        Currency = initialCurrency;
    }

    private void Start()
    {
        Pool = GetComponent<ObjectPool>();
    }

    private void Update()
    {
        HandleEscape();
        CheckWaveEnd();
    }

    public void PickTower(TowerButton clickedButton)
    {
        if (Currency < clickedButton.Price) return;

        startWaveBtn.GetComponent<Button>().interactable = false;
        
        ClickedButton = clickedButton;
        HoverScript.Instance.Activate(clickedButton.Sprite);
    }

    public void SelectTower(Tower tower)
    {
        if (_selectedTower != null)
        {
            _selectedTower.Select();
        }
        _selectedTower = tower;
        _selectedTower.Select();
        upgradePanel.SetActive(true);
        sellTxt.text = $"+ {_selectedTower.Price / 2}<color=lime>$</color>";
        upgradePriceTxt.text = $"-{_selectedTower.NextUpgrade.Price}<color=lime>$</color>";
    }

    public void DeselectTower()
    {
        if (_selectedTower == null) return;
        _selectedTower.Select();
        _selectedTower = null;
        upgradePanel.SetActive(false);
    }

    public void IncreaseCurrency()
    {
        Currency += 10;
    }

    public void BuyTower()
    {
        startWaveBtn.GetComponent<Button>().interactable = true;
        
        Currency = Currency - ClickedButton.Price;
        HoverScript.Instance.Deactivate();
    }

    public void UpgradeTower()
    {
        if (_selectedTower.Level <= _selectedTower.TowerUpgrades.Length &&
            _currency >= _selectedTower.NextUpgrade.Price)
        {
            _selectedTower.Upgrade();
            upgradePriceTxt.text = $"-{_selectedTower.NextUpgrade.Price}<color=lime>$</color>";
            sellTxt.text = $"+ {_selectedTower.Price / 2}<color=lime>$</color>";
        }
    }

    public void SellTower()
    {
        _selectedTower.Sell();
        DeselectTower();
    }

    public void ToggleShowStatPanel(TowerData towerData)
    {
        statPanel.ToggleShow(towerData);
    }

    private void HandleEscape()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (HoverScript.Instance.IsVisible)
            {
                HoverScript.Instance.Deactivate();
            }
            else
            {
                if (optionsMenu.activeSelf)
                {
                    ReturnToPauseMenu();
                }
                else
                {
                    ShowPauseMenu();
                }
            }
        }
    }

    public void StartWave()
    {
        _waveNumber++;
        SpawnMonster = _waveNumber;
        
        waveTxt.text = string.Format("Waves: <color=lime>{0}</color>", _waveNumber);
        startWaveBtn.SetActive(false);
        towerPanel.SetActive(false);
        
        LevelManager.Instance.GeneratePath();
        
        StartCoroutine(SpawnWave());
    }

    private IEnumerator SpawnWave()
    {
        for (int i = 0; i < _waveNumber; i++)
        {
            var monsterIndex = Random.Range(0, 3);
            var type = string.Empty;

            switch (monsterIndex)
            {
                case 0:
                    type = "Minotaur01";
                    break;
                case 1:
                    type = "Minotaur02";
                    break;
                case 2:
                    type = "Minotaur03";
                    break;
            }
            var newMonster = Pool.GetObject(type).GetComponent<Monster>();

            newMonster.Spawn(monsterHealth);

            if (_waveNumber % 3 == 0)
            {
                monsterHealth += 5;
            }
            
            yield return new WaitForSeconds(spawnRate);
        }
    }

    public void CheckWaveEnd()
    {
        if (SpawnMonster != 0) return;
        
        startWaveBtn.SetActive(true);
        towerPanel.SetActive(true);
    }

    private void ShowGameOver()
    {
        SoundManager.Instance.ChangeMusic("GameOverMusic");
        gameOverUI.SetActive(true);
        Time.timeScale = 0;
    }

    public void ShowPauseMenu()
    {
        pauseMenu.SetActive(!pauseMenu.activeSelf);
        Time.timeScale = pauseMenu.activeSelf ? 0 : 1;
    }

    public void ShowOptionsMenu()
    {
        pauseMenu.SetActive(false);
        optionsMenu.SetActive(true);
    }

    public void ReturnToPauseMenu()
    {
        pauseMenu.SetActive(true);
        optionsMenu.SetActive(false);
    }

    public void RestartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        Time.timeScale = 1;
    }

    public void ReturnToMainMenu()
    {
        SceneManager.LoadScene(0);
        Time.timeScale = 1;
    }
}
