﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoverScript : Singleton<HoverScript>
{
    private SpriteRenderer _spriteRenderer;
    private SpriteRenderer _towerRange;

    public bool IsVisible => _spriteRenderer.isVisible;

    private void Awake()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
        _towerRange = transform.GetChild(0).GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    private void Update()
    {
        FollowMouse();
    }

    private void FollowMouse()
    {
        if (!_spriteRenderer.enabled) return;
        transform.position = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        transform.position = new Vector3(transform.position.x, transform.position.y, 0);
    }
    
    public void Activate(Sprite sprite)
    {
        var range = GameManager.Instance.ClickedButton.Tower.Range;
        _towerRange.transform.localScale = Vector3.one * range;
        
        _spriteRenderer.enabled = true;
        _towerRange.enabled = true;
        _spriteRenderer.sprite = sprite;
    }

    public void Deactivate()
    {
        _spriteRenderer.enabled = false;
        _towerRange.enabled = false;
        GameManager.Instance.ClickedButton = null;
    }
}
