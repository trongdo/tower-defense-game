﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class TileScript : MonoBehaviour
{
    private Tower _tower;
    public Point GridPosition { get; private set; }
    public bool Walkable { get; set; }
    public bool IsEmpty { get; set; }
    private Color32 _fullColor = new Color32(255, 118, 118, 255);
    private Color32 _emptyColor = new Color32(96, 255, 90, 255);
    private SpriteRenderer _spriteRenderer;

    private void Awake()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
    }

    public Vector2 WorldPosition => GetComponent<SpriteRenderer>().bounds.center;

    public void Setup(Point gridPosition, Vector3 worldPosition, Transform parent)
    {
        GridPosition = gridPosition;
        var tileTransform = transform;
        tileTransform.position = worldPosition;
        tileTransform.parent = parent;
        LevelManager.Instance.Tiles.Add(gridPosition, this);
        IsEmpty = true;
        Walkable = true;
    }

    private void OnMouseOver()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (!EventSystem.current.IsPointerOverGameObject() && GameManager.Instance.ClickedButton != null && IsEmpty)
            {
                PlaceATower();
            }
            else if (!EventSystem.current.IsPointerOverGameObject() && GameManager.Instance.ClickedButton == null)
            {
                if (_tower != null)
                {
                    GameManager.Instance.SelectTower(_tower);
                }
                else
                {
                    GameManager.Instance.DeselectTower();
                }
            }
        }

        if (!EventSystem.current.IsPointerOverGameObject() && GameManager.Instance.ClickedButton != null)
        {
            ColorTile(IsEmpty ? _emptyColor : _fullColor);
        }

        if (EventSystem.current.IsPointerOverGameObject() && GameManager.Instance.ClickedButton != null)
        {
            if (_spriteRenderer.color == Color.white) return;
            ColorTile(Color.white);
        }
    }

    private void OnMouseExit()
    {
        ColorTile(Color.white);
    }

    private void PlaceATower()
    {
        Walkable = false;
        if (AStar.GetPath(LevelManager.Instance.RedPortalPos, LevelManager.Instance.BluePortalPos) == null)
        {
            Walkable = true;
            return;
        }
        var newTower = Instantiate(GameManager.Instance.ClickedButton.Tower, WorldPosition, Quaternion.identity);
        newTower.transform.parent = transform;
        newTower.GetComponent<Tower>().Tile = this;
        newTower.GetComponent<SpriteRenderer>().sortingOrder = GridPosition.Y;

        _tower = newTower.GetComponent<Tower>();
        
        GameManager.Instance.BuyTower();
        
        ColorTile(Color.white);
        IsEmpty = false;
        Walkable = false;
    }

    private void ColorTile(Color32 newColor)
    {
        _spriteRenderer.color = newColor;
    }
}
