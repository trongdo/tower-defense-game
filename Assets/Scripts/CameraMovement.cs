﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    [SerializeField] private float _cameraSpeed = 0;
    
    private float _xMax = 0f;
    private float _yMin = 0f;

    
    // Start is called before the first frame update
    // void Start()
    // {
    //     
    // }

    // Update is called once per frame
    void Update()
    {
        GetInput();
    }

    private void GetInput()
    {
        if (Input.GetKey(KeyCode.W))
        {
            transform.Translate(Vector3.up * _cameraSpeed * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.S))
        {
            transform.Translate(Vector3.down * _cameraSpeed * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.A))
        {
            transform.Translate(Vector3.left * _cameraSpeed * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.D))
        {
            transform.Translate(Vector3.right * _cameraSpeed * Time.deltaTime);
        }

        transform.position = new Vector3(Mathf.Clamp(transform.position.x, 0, _xMax),
            Mathf.Clamp(transform.position.y, _yMin, 0), -10);
    }
    
    public void SetCameraLimit(Vector3 maxTilePos)
    {
        var worldPoint = Camera.main.ViewportToWorldPoint(new Vector3(1, 0));
        _xMax = maxTilePos.x - worldPoint.x;
        _yMin = maxTilePos.y - worldPoint.y;
    }
}
