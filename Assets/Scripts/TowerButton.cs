﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class TowerButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField] private Sprite _sprite;
    [SerializeField] private Text _priceTxt;
    [SerializeField] private TowerData towerData;
    
    private int _price;
    private Image _image;
    private Tower _tower;

    public int Price
    {
        get => _price;

        private set => _price = value;
    }

    public Sprite Sprite => _sprite;

    public Tower Tower => towerData.Tower;

    private void Awake()
    {
        _tower = towerData.Tower;
        _price = _tower.Price;
        _priceTxt.text = Price + "<color=lime>$</color>";
        _image = GetComponent<Image>();
    }
    
    private void Start()
    {
        GameManager.Instance.OnCurrencyChanged += CheckCurrency_CurrencyChanged;
    }

    private void CheckCurrency_CurrencyChanged()
    {
        CheckCurrency();
    }

    private void CheckCurrency()
    {
        _image.color = GameManager.Instance.Currency >= Price ? Color.white : Color.gray;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        GameManager.Instance.ToggleShowStatPanel(towerData);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        GameManager.Instance.ToggleShowStatPanel(towerData);
    }
}
