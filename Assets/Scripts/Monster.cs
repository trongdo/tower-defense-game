﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Monster : MonoBehaviour
{
    [SerializeField] private float maxSpeed;
    private float _speed;
    private SpriteRenderer _spriteRenderer;
    private Stack<Node> _path;
    private Vector3 _destination;
    private Animator _animator;
    private bool _isActive;
    
    private List<Debuff> _debuffs = new List<Debuff>();
    private List<Debuff> _debuffsToRemove = new List<Debuff>();
    private List<Debuff> _newDebuffs = new List<Debuff>();

    private HealthSystem _health;

    public bool IsAlive => _health.CurrentValue > 0 && gameObject.activeSelf;

    public float Speed
    {
        get => _speed;
        set => _speed = value;
    }

    public float MaxSpeed => maxSpeed;
    
    private static List<Monster> MonsterList = new List<Monster>();

    private void Awake()
    {
        MonsterList.Add(this);
        _animator = GetComponent<Animator>();
        _spriteRenderer = GetComponent<SpriteRenderer>();
        _speed = maxSpeed;
        _health = GetComponent<HealthSystem>();
    }

    private void OnDestroy()
    {
        MonsterList.Clear();
    }

    public void Spawn(int health)
    {
        _path = LevelManager.Instance.Path;
        _isActive = false;
        transform.position = LevelManager.Instance.RedPortal.transform.position;
        StartCoroutine(Scale(new Vector3(0.1f, 0.1f, 1), new Vector3(1, 1, 1)));
        _health.MaxValue = health;
        
        SetPath();
    }

    private void Update()
    {
        Move();
        Animate();
        HandleDebuff();
    }

    public static Monster GetClosestMonster(Vector3 position, float maxRange)
    {
        Monster closestMonster = null;
        foreach (var monster in MonsterList.Where(monster => monster.IsAlive).Where(monster => Vector3.Distance(position, monster.GetPosition()) < maxRange))
        {
            if (closestMonster == null)
            {
                closestMonster = monster;
            }
            else
            {
                if (Vector3.Distance(position, monster.GetPosition()) <
                    Vector3.Distance(position, closestMonster.GetPosition()))
                {
                    closestMonster = monster;
                }
            }
        }
        return closestMonster;
    }

    public Vector3 GetPosition()
    {
        return transform.position;
    }

    private IEnumerator Scale(Vector3 from, Vector3 to, bool despawn = false)
    {
        float progress = 0;

        while (progress <= 1)
        {
            transform.localScale = Vector3.Lerp(from, to, progress);
            progress += Time.deltaTime;

            yield return null;
        }
        
        transform.localScale = to;
        _isActive = true;

        if (despawn)
        {
            Deactivate();
        }
        
    }

    private void SetPath()
    {
        _destination = _path.Pop().WorldPosition;
    }

    private void Move()
    {
        if (!_isActive) return;

            transform.position = Vector3.MoveTowards(transform.position, _destination, _speed * Time.deltaTime);

        if (transform.position != _destination) return;
        if (_path.Count == 0)
        {
            _isActive = false;
            return;
        }
        
        _destination = _path.Pop().WorldPosition;
    }

    private void Animate()
    {
        _animator.SetBool("moving", _isActive);
        if (transform.position.x > _destination.x)
        {
            _spriteRenderer.flipX = true;
        }
        else
        {
            _spriteRenderer.flipX = false;
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("BluePortal"))
        {
            StartCoroutine(Scale(new Vector3(1, 1, 1), new Vector3(0.1f, 0.1f, 0.1f), true));
        }
    }

    private void Deactivate()
    {
        _debuffs.Clear();
        _debuffsToRemove.Clear();
        _newDebuffs.Clear();
        
        _isActive = false;
        _path = null;
        
        GameManager.Instance.Currency--;
        GameManager.Instance.Pool.ReturnObject(gameObject);
        GameManager.Instance.SpawnMonster -= 1;
    }

    public void TakeDamage(float amount)
    {
        if (!_isActive) return;
        _health.DecreaseHealth(amount);
        
        if (_health.CurrentValue > 0) return;
        GameManager.Instance.Currency += 2;
        Deactivate();
    }

    public void AddDebuff(Debuff debuff)
    {
        if (!_debuffs.Exists(x => x.GetType() == debuff.GetType()))
        {
            _newDebuffs.Add(debuff);
        }
    }

    public void RemoveDebuff(Debuff debuff)
    {
        _debuffsToRemove.Add(debuff);
    }

    private void HandleDebuff()
    {
        if (_newDebuffs.Count > 0)
        {
            _debuffs.AddRange(_newDebuffs);
            _newDebuffs.Clear();
        }

        foreach (var debuff in _debuffsToRemove)
        {
            _debuffs.Remove(debuff);
        }
        _debuffsToRemove.Clear();
        
        if (_debuffs.Count == 0) return;
        foreach (var debuff in _debuffs)
        {
            debuff.Update();
        }
    }
}
