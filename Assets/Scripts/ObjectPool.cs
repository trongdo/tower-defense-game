﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using UnityEngine;
using UnityEngine.Serialization;

public class ObjectPool : MonoBehaviour
{
    [Serializable]
    private struct Pool
    {
        public string nameTag;
        public GameObject prefab;
        public int size;
    }

    [SerializeField] private List<Pool> _pools;
    
    private Dictionary<string, Queue<GameObject>> _poolDictionary;

    private void Start()
    {
        _poolDictionary = new Dictionary<string, Queue<GameObject>>();
        foreach (var pool in _pools)
        {
            Queue<GameObject> objectPool = new Queue<GameObject>();
            for (int i = 0; i < pool.size; i++)
            {
                var obj = Instantiate(pool.prefab);
                obj.name = pool.nameTag;
                obj.SetActive(false);
                objectPool.Enqueue(obj);
            }
            _poolDictionary.Add(pool.nameTag, objectPool);
        }
    }

    public GameObject GetObject(string nameTag)
    {
        var objectPool = _poolDictionary[nameTag];
        if (objectPool.Count == 0)
        {
            return CreateNewGameObject(nameTag);
        }

        var obj =  objectPool.Dequeue();
        obj.SetActive(true);
        return obj;
    }

    private GameObject CreateNewGameObject(string nameTag)
    {
        foreach (var pool in _pools.Where(pool => pool.nameTag.Equals(nameTag)))
        {
            var newObj = Instantiate(pool.prefab);
            newObj.name = nameTag;
            return newObj;
        }
        return null;
    }

    public void ReturnObject(GameObject gameObj)
    {
        _poolDictionary[gameObj.name].Enqueue(gameObj);
        gameObj.SetActive(false);
    }
}
